Blockly.Blocks['dierct'] = {
    init: function() {
        this.appendDummyInput()
            .appendField("向")
            .appendField(new Blockly.FieldDropdown([
                ["前", "forward"],
                ["后", "backward"],
                ["左", "left"],
                ["右", "right"],
                ["左前", "left_forw"],
                ["右前", "right_forw"],
                ["左后", "left_back"],
                ["右后", "right_back"]
            ]), "direction");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(165);
        this.setTooltip("设置小车行进方向");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['speed'] = {
    init: function() {
        this.appendValueInput("speed")
            .setCheck("Number")
            .appendField("小车速度为");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(165);
        this.setTooltip("设置小车速度");
        this.setHelpUrl("");
    }
};